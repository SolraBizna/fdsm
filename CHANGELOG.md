# Changelog

## 0.1.1

Fix bug where `correct_sign_mtsdf` did not correct the sign of the alpha component.

## 0.1.0

Initial release.
