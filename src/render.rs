//! Rendering from distance fields.

use image::{GenericImage, GenericImageView, Luma, Pixel, Primitive, Rgb, Rgba};
use na::{Point2, Scale2};
use num_traits::cast::{NumCast, ToPrimitive};

use crate::{
    bezier::{
        prepared::{PreparedColoredShape, PreparedComponent},
        scanline::FillRule,
    },
    generate::{pixel_from_f64, pixel_value_to_signed_distance, render},
};

fn mix(a1: f64, a2: f64, t: f64) -> f64 {
    a1 * (1.0 - t) + a2 * t
}

fn sample_bilinear<P: Pixel>(input: &impl GenericImageView<Pixel = P>, p: Point2<f64>) -> P
where
    P::Subpixel: Primitive,
{
    let x1 = (p.x - 0.5).floor();
    let y1 = (p.y - 0.5).floor();
    let x2 = x1 + 1.0;
    let y2 = y1 + 1.0;
    let wx = p.x - x1 - 0.5;
    let wy = p.y - y1 - 0.5;
    let x1 = x1 as u32;
    let y1 = y1 as u32;
    let x2 = (x2 as u32).min(input.width() - 1);
    let y2 = (y2 as u32).min(input.height() - 1);
    let ll = input.get_pixel(x1, y1);
    let ul = input.get_pixel(x2, y1);
    let lu = input.get_pixel(x1, y2);
    let uu = input.get_pixel(x2, y2);
    let mut out = ll;
    for (i, out) in out.channels_mut().iter_mut().enumerate() {
        let ll = ll.channels()[i].to_f64().unwrap();
        let ul = ul.channels()[i].to_f64().unwrap();
        let lu = lu.channels()[i].to_f64().unwrap();
        let uu = uu.channels()[i].to_f64().unwrap();
        let x = mix(mix(ll, ul, wx), mix(lu, uu, wx), wy);
        *out = <P::Subpixel as NumCast>::from(x).unwrap();
    }
    out
}

fn median<P: Primitive>(sample: Rgb<P>) -> P {
    let [r, g, b] = sample.0;
    fn mn<P: Primitive>(x: P, y: P) -> P {
        if x < y {
            x
        } else {
            y
        }
    }
    fn mx<P: Primitive>(x: P, y: P) -> P {
        if x > y {
            x
        } else {
            y
        }
    }
    mx(mn(r, g), mn(mx(r, g), b))
}

fn render_generic<In: Pixel, F: Fn(In) -> In::Subpixel>(
    input: &impl GenericImageView<Pixel = In>,
    output: &mut impl GenericImage<Pixel = Luma<u8>>,
    subpixel: F,
    px_range: f64,
) {
    let thresh = input.width() as f64 / output.width() as f64;
    render(
        &na::convert(Scale2::new(
            1.0 / output.width() as f64,
            1.0 / output.height() as f64,
        )),
        |point| {
            let sample = sample_bilinear(
                input,
                Point2::new(
                    point.x * input.width() as f64,
                    point.y * input.height() as f64,
                ),
            );
            let sample = subpixel(sample);
            let distance = pixel_value_to_signed_distance(sample, px_range);
            let w = (distance / thresh).clamp(-1.0, 1.0);
            let w = (1.0 + w) / 2.0;
            Luma([pixel_from_f64(w)])
        },
        output,
    )
}

/// Renders a raster image of a shape from its MSDF.
pub fn render_msdf(
    input: &impl GenericImageView<Pixel = Rgb<u8>>,
    output: &mut impl GenericImage<Pixel = Luma<u8>>,
    px_range: f64,
) {
    render_generic(input, output, median, px_range)
}

/// Renders a raster image of a shape from its SDF.
pub fn render_sdf(
    input: &impl GenericImageView<Pixel = Luma<u8>>,
    output: &mut impl GenericImage<Pixel = Luma<u8>>,
    px_range: f64,
) {
    render_generic(input, output, |sample| sample.0[0], px_range)
}

/// Corrects the sign of a signed distance field, so that points inside
/// the shape have positive values and points outside the shape have
/// negative values.
pub fn correct_sign_sdf<P: Primitive>(
    sdf: &mut impl GenericImage<Pixel = Luma<P>>,
    shape: &PreparedComponent,
    fill_rule: FillRule,
) {
    let half: P = pixel_from_f64(0.5);
    for y in 0..sdf.height() {
        let scanline = shape.scanline(y as f64 + 0.5);
        let mut cursor = scanline.cursor();
        for x in 0..sdf.width() {
            let fill = cursor.filled(x as f64 + 0.5, fill_rule);
            let mut sd = sdf.get_pixel(x, y);
            if (sd.0[0] > half) != fill {
                sd.invert();
                sdf.put_pixel(x, y, sd);
            }
        }
    }
}

/// Corrects the sign of a multi-channel signed distance field, so
/// that points inside the shape have positive values and points
/// outside the shape have negative values.
pub fn correct_sign_msdf<P: Primitive>(
    sdf: &mut impl GenericImage<Pixel = Rgb<P>>,
    shape: &PreparedColoredShape,
    fill_rule: FillRule,
) where
    Rgb<P>: Pixel,
{
    correct_sign_msdf_generic(sdf, shape, fill_rule);
}

/// Corrects the sign of a combined multi-channel and true signed
/// distance field, so that points inside the shape have positive
/// values and points outside the shape have negative values.
pub fn correct_sign_mtsdf<P: Primitive>(
    sdf: &mut impl GenericImage<Pixel = Rgba<P>>,
    shape: &PreparedColoredShape,
    fill_rule: FillRule,
) where
    Rgba<P>: Pixel,
{
    correct_sign_msdf_generic(sdf, shape, fill_rule);
}

fn correct_sign_msdf_generic<P: Primitive, Px: Pixel<Subpixel = P>>(
    sdf: &mut impl GenericImage<Pixel = Px>,
    shape: &PreparedColoredShape,
    fill_rule: FillRule,
) {
    let half: P = pixel_from_f64(0.5);
    let w = sdf.width();
    let h = sdf.height();
    if w * h == 0 {
        return;
    }
    let mut ambiguous = false;
    let mut match_map = vec![0i8; (w * h) as usize];
    let mut match_idx = 0;
    for y in 0..h {
        let scanline = shape.scanline(y as f64 + 0.5);
        let mut cursor = scanline.cursor();
        for x in 0..w {
            let fill = cursor.filled(x as f64 + 0.5, fill_rule);
            let mut msd = sdf.get_pixel(x, y);
            let sd = median(msd.to_rgb());
            if sd == half {
                ambiguous = true;
            } else if (sd > half) != fill {
                msd.invert();
                match_map[match_idx] = -1;
            } else {
                match_map[match_idx] = 1;
            }
            if msd.channels().len() >= 4 {
                let alpha = &mut msd.channels_mut()[3];
                if (*alpha > half) != fill {
                    *alpha = P::DEFAULT_MAX_VALUE - *alpha;
                }
            }
            sdf.put_pixel(x, y, msd);
            match_idx += 1;
        }
    }
    if ambiguous {
        match_idx = 0;
        for y in 0..h {
            for x in 0..w {
                if match_map[match_idx] == 0 {
                    let mut neighbor_match = 0;
                    if x > 0 {
                        neighbor_match += match_map[match_idx - 1];
                    }
                    if x < w - 1 {
                        neighbor_match += match_map[match_idx + 1];
                    }
                    if y > 0 {
                        neighbor_match += match_map[match_idx - w as usize];
                    }
                    if y < h - 1 {
                        neighbor_match += match_map[match_idx + w as usize];
                    }
                    if neighbor_match < 0 {
                        let mut msd = sdf.get_pixel(x, y);
                        msd.invert();
                        sdf.put_pixel(x, y, msd);
                    }
                }
                match_idx += 1;
            }
        }
    }
}
